package com.technokratos.api;

import com.technokratos.dto.request.UserRequest;
import com.technokratos.dto.response.UserResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Set;
import java.util.UUID;

@Api(tags = "Users | Пользователи", value = "Юзер")
@RequestMapping("/api/v1/users")
public interface UserApi {

    @GetMapping("/{user-id}")
    @ResponseStatus(HttpStatus.OK)
    UserResponse getById(@PathVariable("user-id") UUID uuid);

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    Set<UserResponse> getAll();

    @ApiOperation(value = "Создание юзера", nickname = "user-create", response = UUID.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Юзер создан", response = UUID.class),
            @ApiResponse(code = 400, message = "Ошибка валидации"),
            @ApiResponse(code = 500, message = "Серверная ошибка")
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    UUID create(@RequestBody UserRequest userRequest);
}
