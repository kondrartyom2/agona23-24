package com.technokratos.service;

import com.technokratos.dto.request.UserRequest;
import com.technokratos.dto.response.UserResponse;

import java.util.Set;
import java.util.UUID;

public interface UserService {
    UserResponse getById(UUID uuid);

    Set<UserResponse> getAll();

    UUID create(UserRequest userRequest);
}
