package com.technokratos.controller;

import com.technokratos.api.UserApi;
import com.technokratos.dto.request.UserRequest;
import com.technokratos.dto.response.UserResponse;
import com.technokratos.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService service;
    @Override
    public UserResponse getById(UUID uuid) {
        return service.getById(uuid);
    }

    @Override
    public Set<UserResponse> getAll() {
        return service.getAll();
    }

    @Override
    public UUID create(UserRequest userRequest) {
        return service.create(userRequest);
    }
}
