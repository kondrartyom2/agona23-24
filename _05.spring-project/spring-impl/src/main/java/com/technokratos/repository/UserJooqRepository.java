package com.technokratos.repository;

import com.technokratos.model.jooq.schema.tables.pojos.AccountEntity;

import java.util.Optional;
import java.util.UUID;

interface UserJooqRepository {
    Optional<AccountEntity> findById(UUID id);
}
