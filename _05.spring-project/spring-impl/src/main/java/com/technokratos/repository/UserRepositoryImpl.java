package com.technokratos.repository;

import com.technokratos.model.UserEntity;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private final JdbcTemplate jdbcTemplate;

    private final static String SQL_GET_BY_ID = "select * from account where id = '%s'";

    private RowMapper<UserEntity> rowMapper = (rs, rowNum) -> UserEntity.builder()
            .name(rs.getString("name"))
            .build();

    @Override
    public Optional<UserEntity> findById(UUID uuid) {
        try (val stream = jdbcTemplate.queryForStream(SQL_GET_BY_ID.formatted(uuid), rowMapper)) {
            return stream.findAny();
        }
    }
}
