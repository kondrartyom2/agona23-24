package com.technokratos.repository;

import com.technokratos.model.jooq.schema.Tables;
import com.technokratos.model.jooq.schema.tables.pojos.AccountEntity;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class UserJooqRepositoryImpl implements UserJooqRepository {

    private final DSLContext jooq;

    @Override
    public Optional<AccountEntity> findById(final UUID id) {
        return jooq.select(Tables.ACCOUNT_ENTITY.fields())
                .from(Tables.ACCOUNT_ENTITY)
                .where(Tables.ACCOUNT_ENTITY.ID.eq(id))
                .fetchOptionalInto(AccountEntity.class);
    }
}
