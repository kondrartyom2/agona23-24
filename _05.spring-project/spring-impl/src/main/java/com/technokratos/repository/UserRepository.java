package com.technokratos.repository;

import com.technokratos.model.UserEntity;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository {
    Optional<UserEntity> findById(UUID uuid);
}
