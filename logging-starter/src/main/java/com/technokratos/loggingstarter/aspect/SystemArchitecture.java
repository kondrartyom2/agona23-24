package com.technokratos.loggingstarter.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SystemArchitecture {

    @Pointcut("execution(* com.technokratos..security..*(..))")
    public void securityMethods() {

    }

    @Pointcut("within(com.technokratos..*) && !securityMethods()")
    public void allMethods() {
    }

    @Pointcut("bean(*Controller) && !securityMethods()")
    public void controllerMethods() {
    }

    @Pointcut("within(org.springframework.web.bind.annotation.RestController) && !securityMethods()")
    public void restControllerMethods() {
    }

    @Pointcut("allMethods() && !controllerMethods() && !securityMethods()")
    public void allWithoutControllerMethods() {
    }

    @Pointcut("allMethods() && !restControllerMethods() && !securityMethods()")
    public void allWithoutRestControllerMethods() {
    }

    @Pointcut("@annotation(com.technokratos.loggingstarter.annotation.MethodLogging) && !securityMethods()")
    public void methodLoggingMethods() {
    }
}