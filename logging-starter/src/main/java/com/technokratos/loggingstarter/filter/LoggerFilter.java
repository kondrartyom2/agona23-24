package com.technokratos.loggingstarter.filter;

import org.jboss.logging.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static com.technokratos.loggingstarter.config.constant.LoggerConstant.HTTP_HEADERS_REQUEST_ID;
import static com.technokratos.loggingstarter.config.constant.LoggerConstant.STRUCTURED_ARGUMENTS_KEY_REQUEST_URI;

public class LoggerFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException, ServletException, IOException {
        String requestId = Optional.ofNullable(request.getHeader(HTTP_HEADERS_REQUEST_ID))
                .orElse(UUID.randomUUID().toString());

        MDC.put(STRUCTURED_ARGUMENTS_KEY_REQUEST_URI, request.getMethod() + " " + request.getRequestURI());
        MDC.put(HTTP_HEADERS_REQUEST_ID, requestId);

        response.setHeader(HTTP_HEADERS_REQUEST_ID, requestId);

        filterChain.doFilter(request, response);
    }
}