package com.technokratos.loggingstarter.config;

import com.technokratos.loggingstarter.interceptor.resttemplate.LoggerRestTemplateClientInterceptor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    @Bean
    public RestTemplate getRestTemplate(LoggerRestTemplateClientInterceptor loggerInterceptor) {
        return new RestTemplateBuilder()
                .additionalInterceptors(loggerInterceptor)
                .build();
    }
}
