package com.technokratos.loggingstarter.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "technokratos.logger")
public class LoggerProperties {

    private Boolean enabled;
}
