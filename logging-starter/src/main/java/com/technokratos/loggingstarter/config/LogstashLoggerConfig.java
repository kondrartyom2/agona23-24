package com.technokratos.loggingstarter.config;

import com.technokratos.loggingstarter.aspect.SystemArchitecture;
import com.technokratos.loggingstarter.interceptor.resttemplate.LoggerRestTemplateClientInterceptor;
import com.technokratos.loggingstarter.interceptor.resttemplate.LoggerRestTemplateClientInterceptorLogstash;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.argument.StructuredArguments;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.technokratos.loggingstarter.config.constant.LoggerConstant.HTTP_HEADERS_REQUEST_ID;
import static com.technokratos.loggingstarter.config.constant.LoggerConstant.STRUCTURED_ARGUMENTS_KEY_ARGS;
import static com.technokratos.loggingstarter.config.constant.LoggerConstant.STRUCTURED_ARGUMENTS_KEY_EXCEPTION;
import static com.technokratos.loggingstarter.config.constant.LoggerConstant.STRUCTURED_ARGUMENTS_KEY_METHOD;
import static com.technokratos.loggingstarter.config.constant.LoggerConstant.STRUCTURED_ARGUMENTS_KEY_RESULT;
import static com.technokratos.loggingstarter.config.constant.LoggerConstant.STRUCTURED_ARGUMENTS_KEY_STEP;
import static com.technokratos.loggingstarter.config.constant.LoggerConstant.Step;


@Slf4j
@Aspect
@Configuration
@ConditionalOnProperty(prefix = "technokratos.logger", value = "enabled", havingValue = "true")
public class LogstashLoggerConfig {

    @Bean
    public SystemArchitecture systemArchitecture() {
        return new SystemArchitecture();
    }

    @AfterReturning(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.eventListenerMethods()", returning = "result")
    public void afterLogMethodLoggingEventListener(JoinPoint joinPoint, Object result) {
        afterReturningTemplateLoggingInfo(Step.AFTER_EVENT_LISTENER, joinPoint, result);
    }

    @Before(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.methodLoggingMethods()")
    public void beforeLogMethodLoggingMethods(JoinPoint joinPoint) {
        String requestId = Optional.ofNullable(MDC.get(HTTP_HEADERS_REQUEST_ID))
                .orElse(UUID.randomUUID().toString());
        MDC.put(HTTP_HEADERS_REQUEST_ID, requestId);

        beforeTemplateLoggingInfo(Step.BEFORE_METHOD_LOGGING_METHOD, joinPoint);
    }

    @AfterReturning(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.methodLoggingMethods()", returning = "result")
    public void afterLogMethodLoggingMethods(JoinPoint joinPoint, Object result) {
        afterReturningTemplateLoggingInfo(Step.AFTER_METHOD_LOGGING_METHOD, joinPoint, result);
    }

    @Before(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.restControllerMethods()")
    public void beforeLogControllers(JoinPoint joinPoint) {
        beforeTemplateLoggingInfo(Step.BEFORE_CONTROLLER, joinPoint);
    }

    @AfterReturning(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.restControllerMethods()", returning = "result")
    public void afterLogControllers(JoinPoint joinPoint, Object result) {
        afterReturningTemplateLoggingInfo(Step.AFTER_CONTROLLER, joinPoint, result);
    }

    @Before(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.allWithoutRestControllerMethods()")
    public void beforeLogClientsAndServices(JoinPoint joinPoint) {

        List<Object> args = processMultipartFilesForLog(joinPoint.getArgs());

        log.debug("Aspect logging",
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_STEP, Step.BEFORE_METHOD),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_METHOD, joinPoint.getSignature().toShortString()),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_ARGS, String.valueOf(args))
        );
    }

    @AfterReturning(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.allWithoutRestControllerMethods()", returning = "result")
    public void afterLogClientsAndServices(JoinPoint joinPoint, Object result) {

        log.debug("Aspect logging",
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_STEP, Step.AFTER_METHOD),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_METHOD, joinPoint.getSignature().toShortString()),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_RESULT, processResultObject(result))
        );
    }

    @AfterThrowing(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.allMethods()", throwing = "exception")
    public void afterThrowingLog(JoinPoint joinPoint, Throwable exception) {

        log.error("Aspect logging",
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_STEP, Step.AFTER_THROWING),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_METHOD, joinPoint.getSignature().toShortString()),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_EXCEPTION, exception.getMessage()),
                exception);
    }

    private void beforeTemplateLoggingInfo(String step, JoinPoint joinPoint) {
        List<Object> args = processMultipartFilesForLog(joinPoint.getArgs());

        log.info("Aspect logging",
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_STEP, step),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_METHOD, joinPoint.getSignature().toShortString()),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_ARGS, String.valueOf(args))
        );
    }

    private void afterReturningTemplateLoggingInfo(String step, JoinPoint joinPoint, Object result) {
        log.info("Aspect logging",
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_STEP, step),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_METHOD, joinPoint.getSignature().toShortString()),
                StructuredArguments.v(STRUCTURED_ARGUMENTS_KEY_RESULT, processResultObject(result))
        );
    }

    private String processResultObject(Object result) {
        String toStringObject = String.valueOf(result);
        int toStringObjectLength = toStringObject.length();
        int maxToStringObjectLength = 3000;

        if (toStringObjectLength > maxToStringObjectLength) {
            return String.format("Length is too long (%s); %s", toStringObjectLength, toStringObject.substring(0, maxToStringObjectLength));
        }

        return toStringObject;
    }

    //TODO: looks like a crutch
    private List<Object> processMultipartFilesForLog(Object[] args) {
        return Arrays.stream(args)
                .map(o -> {
                    if (o instanceof MultipartFile) {
                        return ((MultipartFile) o).getOriginalFilename();
                    }
                    return o;
                })
                .collect(Collectors.toList());
    }
    @Bean
    public LoggerRestTemplateClientInterceptor logstashRestTemplateClientInterceptor() {
        return new LoggerRestTemplateClientInterceptorLogstash();
    }
}
