package com.technokratos.loggingstarter.config;

import com.technokratos.loggingstarter.interceptor.LoggerInterceptor;
import com.technokratos.loggingstarter.interceptor.feign.LoggerFeignClientInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Bean
    public LoggerInterceptor loggerInterceptor() {
        return new LoggerInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loggerInterceptor());
    }

    @Bean
    public LoggerFeignClientInterceptor requestInterceptor() {
        return new LoggerFeignClientInterceptor();
    }
}