package com.technokratos.loggingstarter.config;

import com.technokratos.loggingstarter.aspect.SystemArchitecture;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.technokratos.loggingstarter.config.ConsoleLoggerConfig.LogPrefix.AFTER_RETURNING;
import static com.technokratos.loggingstarter.config.ConsoleLoggerConfig.LogPrefix.AFTER_THROWING;
import static com.technokratos.loggingstarter.config.ConsoleLoggerConfig.LogPrefix.BEFORE;


@Aspect
@Slf4j
@Configuration
@ConditionalOnProperty(prefix = "technokratos.logger", value = "enabled", havingValue = "false", matchIfMissing = true)
public class ConsoleLoggerConfig {

    @Bean
    public SystemArchitecture systemArchitecture() {
        return new SystemArchitecture();
    }

    private static final String BEFORE_METHOD_MESSAGE_PATTERN = "%s method [{}] call args: [{}]".formatted(BEFORE.getLogPrefix());
    private static final String AFTER_METHOD_MESSAGE_PATTERN = AFTER_RETURNING.getLogPrefix() + " method [{}] result: [{}]";
    private static final String AFTER_THROWING_MESSAGE_PATTERN = AFTER_THROWING.getLogPrefix() + " method [{}] throw exception";

    @Before(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.allMethods()")
    public void beforeLogControllers(JoinPoint joinPoint) {
        log.info(BEFORE_METHOD_MESSAGE_PATTERN, joinPoint.getSignature(), joinPoint.getArgs());
    }

    @AfterReturning(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.controllerMethods()", returning = "result")
    public void afterLogControllers(JoinPoint joinPoint, Object result) {
        log.info(AFTER_METHOD_MESSAGE_PATTERN, joinPoint.getSignature(), result);
    }

    @Before(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.allWithoutControllerMethods()")
    public void beforeLogClientsAndServices(JoinPoint joinPoint) {
        log.debug(BEFORE_METHOD_MESSAGE_PATTERN, joinPoint.getSignature(), joinPoint.getArgs());
    }

    @AfterReturning(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.allWithoutControllerMethods()", returning = "result")
    public void afterLogClientsAndServices(JoinPoint joinPoint, Object result) {
        log.debug(AFTER_METHOD_MESSAGE_PATTERN, joinPoint.getSignature(), result);
    }

    @AfterThrowing(value = "com.technokratos.loggingstarter.aspect.SystemArchitecture.allMethods()", throwing = "exception")
    public void afterThrowingLog(JoinPoint joinPoint, Throwable exception) {
        log.error(AFTER_THROWING_MESSAGE_PATTERN, joinPoint.getSignature(), exception);
    }

    @RequiredArgsConstructor
    @Getter
    public enum LogPrefix {
        BEFORE("====@Before===="),
        AFTER_RETURNING("====@AfterReturning===="),
        AFTER_THROWING("====@AfterThrowing====");

        final String logPrefix;
    }
}
