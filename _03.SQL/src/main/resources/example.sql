create sequence account_id_sequence
    start with 100000
    increment by 1
    cache 50;

create table account (
    id bigint default nextval('account_id_sequence') not null,
    name varchar(50) not null default '-',
    phone varchar(25) not null,

    constraint account_id_pk primary key(id),
    constraint account_phone_uc unique(phone)
);

comment on table account is 'Пользователь';
comment on column account.phone is 'Номер телефона';

insert into account(name, phone) values ('Artem', '89600872203');

alter table account add constraint
    account_name_lower_ch check (name = lower(name));

create extension if not exists "uuid-ossp";
create table music (
    uuid uuid not null default uuid_generate_v4() primary key,
    name varchar not null,
    phone varchar not null unique
);

alter table music add column
    account_id bigint
        constraint music_account_id_fk references account(id);

alter table music drop column phone;
alter table music add column duration int;
alter table music add column album varchar(20);

select * from account ac
    join music m on ac.id = m.account_id;