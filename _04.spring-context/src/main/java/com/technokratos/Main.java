package com.technokratos;

import com.technokratos.config.ApplicationConfig;
import com.technokratos.service.StringGenerator;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext configurableApplicationContext = new AnnotationConfigApplicationContext();
        configurableApplicationContext.register(ApplicationConfig.class);
        configurableApplicationContext.refresh();
        StringGenerator uuidStringGenerator = configurableApplicationContext.getBean("uuidStringGenerator", StringGenerator.class);
        StringGenerator numberStringGenerator = configurableApplicationContext.getBean("numberStringGenerator", StringGenerator.class);
        System.out.println(uuidStringGenerator.generate());
        System.out.println(numberStringGenerator.generate());
    }
}