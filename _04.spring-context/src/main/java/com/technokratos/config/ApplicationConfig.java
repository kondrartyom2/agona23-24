package com.technokratos.config;

import com.technokratos.service.NumberStringGenerator;
import com.technokratos.service.Randomizer;
import com.technokratos.service.RandomizerImpl;
import com.technokratos.service.StringGenerator;
import com.technokratos.service.UuidStringGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(basePackages = "com.technokratos")
@PropertySource("classpath:/application.properties")
public class ApplicationConfig {

    @Bean
    @Scope(scopeName = "prototype")
    public Randomizer<Integer> randomizer() {
        return new RandomizerImpl();
    }

    @Bean
    public StringGenerator uuidStringGenerator() {
        return new UuidStringGenerator();
    }

    @Bean
    public StringGenerator numberStringGenerator() {
        return new NumberStringGenerator(randomizer());
    }
}
