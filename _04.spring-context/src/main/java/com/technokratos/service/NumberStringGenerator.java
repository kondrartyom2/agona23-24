package com.technokratos.service;

import org.springframework.beans.factory.annotation.Autowired;

public class NumberStringGenerator implements StringGenerator {

//    @Autowired
//    private Randomizer<Integer> randomizer;
    private final Randomizer<Integer> randomizer;

    public NumberStringGenerator(@Autowired Randomizer<Integer> randomizer) {
        this.randomizer = randomizer;
    }

    @Override
    public String generate() {
        return String.valueOf(randomizer.randomize());
    }
}
