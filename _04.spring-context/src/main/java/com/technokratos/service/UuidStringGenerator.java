package com.technokratos.service;

import java.util.UUID;

public class UuidStringGenerator implements StringGenerator {
    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
