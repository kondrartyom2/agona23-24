package com.technokratos.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomizerImpl implements Randomizer<Integer> {

    @Value("${randomizer.min}")
    private Integer min;
    @Value("${randomizer.max}")
    private Integer max;

    @Override
    public Integer randomize() {
        Random random = new Random();
        return random.nextInt(this.min, this.max);
    }
}
