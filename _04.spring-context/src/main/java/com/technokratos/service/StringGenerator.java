package com.technokratos.service;

public interface StringGenerator {
    String generate();
}
