package com.technokratos.service;

public interface Randomizer<T> {
    T randomize();
}
