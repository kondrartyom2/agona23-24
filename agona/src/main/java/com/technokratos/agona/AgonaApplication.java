package com.technokratos.agona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgonaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgonaApplication.class, args);
    }

}
