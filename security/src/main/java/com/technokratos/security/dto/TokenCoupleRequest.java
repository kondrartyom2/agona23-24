package com.technokratos.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenCoupleRequest {

    private String accessToken;
    private String refreshToken;
}
