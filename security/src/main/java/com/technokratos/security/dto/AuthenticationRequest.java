package com.technokratos.security.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequest {

    //проверка на пустоту
    private String login;
    //проверка на пустоту
    private String password;
}
