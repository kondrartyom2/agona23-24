package com.technokratos.security.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AccountResponse {

    private Long id;
    private String fullName;
    private String login;
    private char[] password;
    private Role role;
}
