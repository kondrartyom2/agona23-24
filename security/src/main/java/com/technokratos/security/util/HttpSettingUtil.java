package com.technokratos.security.util;

import com.technokratos.security.dto.TokenRequest;
import com.technokratos.security.security.exception.AuthenticationHeaderException;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

@UtilityClass
@Slf4j
public class HttpSettingUtil {

    public static final String BEARER = "Bearer";

    public String getTokenFromAuthorizationHeader(String authorizationHeader) {
        return Optional.ofNullable(authorizationHeader)
                .filter(StringUtils::isNotBlank)
                .map(bearer -> StringUtils.removeStart(bearer, BEARER).trim())
                .filter(StringUtils::isNotBlank)
                .orElse(null);
    }
    public TokenRequest getTokenFromValidatedAuthorizationHeader(String authorizationHeader) {

        if (authorizationHeader == null) {
            return null;
        }

        log.info("Loading user for Authorization header: {}", authorizationHeader);

        if (!authorizationHeader.startsWith(BEARER)) {
            throw new AuthenticationHeaderException("Invalid authentication scheme found in Authorization header");
        }

        String token = HttpSettingUtil.getTokenFromAuthorizationHeader(authorizationHeader);
        if (token == null) {
            throw new AuthenticationHeaderException("Authorization header token is empty");
        }

        return TokenRequest.builder().token(token).build();
    }
}
