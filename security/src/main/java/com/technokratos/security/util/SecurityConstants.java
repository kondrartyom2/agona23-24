package com.technokratos.security.util;

import lombok.NoArgsConstructor;
import lombok.experimental.UtilityClass;

@UtilityClass
@NoArgsConstructor
public class SecurityConstants {

    public static final String BEARER = "Bearer ";
    public static final String ROLE = "role";
}
