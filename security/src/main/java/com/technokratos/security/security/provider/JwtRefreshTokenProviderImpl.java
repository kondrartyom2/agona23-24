package com.technokratos.security.security.provider;

import com.technokratos.security.dto.AccountResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class JwtRefreshTokenProviderImpl implements JwtRefreshTokenProvider {

    private final AccountProvider accountProvider;

    @Override
    public String generateRefreshToken(AccountResponse accountResponse) {
//        String role = accountResponse.getRole().name();
//        return String.valueOf(accountProvider.getAccountRefreshTokenService(roles)
//                .generateRefreshToken(accountResponse).getId());
        return UUID.randomUUID().toString();
    }

//    @Override
//    public RefreshTokenEntity verifyRefreshTokenExpiration(String refreshToken, List<String> roles) {
//        return accountProvider.getAccountRefreshTokenService(roles).verifyRefreshTokenExpiryDate(refreshToken);
//    }
}
