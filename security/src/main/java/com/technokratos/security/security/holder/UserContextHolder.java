package com.technokratos.security.security.holder;

import com.technokratos.security.security.userdetails.UserAccount;

public interface UserContextHolder {

    UserAccount getUserAccountFromSecurityContext();
}
