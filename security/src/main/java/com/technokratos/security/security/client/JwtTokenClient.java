package com.technokratos.security.security.client;

import com.technokratos.security.dto.AccountResponse;
import com.technokratos.security.dto.TokenCoupleRequest;
import com.technokratos.security.dto.TokenCoupleResponse;
import com.technokratos.security.dto.TokenRequest;
import com.technokratos.security.service.JwtTokenService;

public class JwtTokenClient {

    private JwtTokenService jwtTokenService;

    public AccountResponse userInfoByToken(TokenRequest token) {
        return jwtTokenService.getUserInfoByToken(token);
    }

    public TokenCoupleResponse updateTokens(TokenCoupleRequest tokenRequest) {
        return jwtTokenService.refreshAccessToken(tokenRequest);
    }
}
