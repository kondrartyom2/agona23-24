package com.technokratos.security.security.provider;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class AccountProviderImpl implements AccountProvider {

    private final DeveloperAccountService developerAccountService;
    private final ExecutiveCommitteeAccountService executiveCommitteeAccountService;
    private final DeveloperAccountRefreshTokenService developerAccountRefreshTokenService;
    private final ExecutiveCommitteeRefreshTokenService executiveCommitteeRefreshTokenService;

    @Override
    public AccountService getAccountService(List<String> roles) {
        if (roles.contains(USER_DEVELOPER.name()))
            return developerAccountService;
        else
            return executiveCommitteeAccountService;
    }

    @Override
    public AccountRefreshTokenService getAccountRefreshTokenService(List<String> roles) {
        if (roles.contains(USER_DEVELOPER.name()))
            return developerAccountRefreshTokenService;
        else
            return executiveCommitteeRefreshTokenService;
    }
}
