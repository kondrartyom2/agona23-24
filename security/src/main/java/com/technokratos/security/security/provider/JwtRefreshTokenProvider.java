package com.technokratos.security.security.provider;

import com.technokratos.security.dto.AccountResponse;

import java.util.List;

public interface JwtRefreshTokenProvider {
    String generateRefreshToken(AccountResponse accountResponse);

//    RefreshTokenEntity verifyRefreshTokenExpiration(String refreshToken, List<String> roles);
}
