package com.technokratos.security.service;

import com.example.springsecurity6authorizationserver.client.ProfileClient;
import com.example.springsecurity6authorizationserver.dto.AuthenticationRequest;
import com.example.springsecurity6authorizationserver.dto.AuthenticationResponse;
import com.example.springsecurity6authorizationserver.dto.Profile;
import com.example.springsecurity6authorizationserver.dto.Role;
import com.nimbusds.jose.util.Pair;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2RefreshToken;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Base64;
import java.util.Set;

import static com.example.springsecurity6authorizationserver.consts.AuthorizationConstants.JWS_HEADER;
import static com.example.springsecurity6authorizationserver.consts.AuthorizationConstants.PROFILE_ID;
import static com.example.springsecurity6authorizationserver.consts.AuthorizationConstants.ROLE;
import static com.example.springsecurity6authorizationserver.consts.AuthorizationConstants.TEST_CLIENT_ID;
import static com.example.springsecurity6authorizationserver.consts.AuthorizationConstants.USERNAME;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final PasswordEncoder passwordEncoder;
    private final JwtEncoder jwtEncoder;
    private final JwtDecoder jwtDecoder;

    public AuthenticationResponse signIn(final AuthenticationRequest authenticationRequest) {
        String login = authenticationRequest.getLogin();
        Profile profile = profileClient.getProfile(login);
        String presentedPassword = authenticationRequest.getPassword();
        String currentPassword = String.valueOf(profile.getPassword());
        checkPassword(presentedPassword, currentPassword);

        RegisteredClient registeredClient = registeredClientRepository.findByClientId(TEST_CLIENT_ID);
        checkRegisterClient(registeredClient, AuthorizationGrantType.JWT_BEARER);

        String tokenId = Base64.getEncoder().encodeToString((profile.getId() + "_" + profile.getLogin()).getBytes());
        Pair<Jwt, Jwt> tokenPair = createAccessAndRefreshTokenPair(registeredClient, profile, tokenId);
        saveOAuth2Authorization(registeredClient, AuthorizationGrantType.JWT_BEARER, tokenId, tokenPair, login);

        String accessToken = tokenPair.getLeft().getTokenValue();
        String refreshToken = tokenPair.getRight().getTokenValue();
        return new AuthenticationResponse(accessToken, refreshToken);
    }

    public AuthenticationResponse refreshToken(@NotBlank final String refreshToken) {
        Jwt verifiedRefreshToken = jwtDecoder.decode(refreshToken);

        RegisteredClient registeredClient = registeredClientRepository.findByClientId(TEST_CLIENT_ID);
        checkRegisterClient(registeredClient, AuthorizationGrantType.REFRESH_TOKEN);

        String tokenId = verifiedRefreshToken.getId();
        Long profileId = verifiedRefreshToken.getClaim(PROFILE_ID);
        Profile profile = profileClient.getProfile(profileId);
        Pair<Jwt, Jwt> tokenPair = createAccessAndRefreshTokenPair(registeredClient, profile, tokenId);

        String login = profile.getLogin();
        saveOAuth2Authorization(registeredClient, AuthorizationGrantType.REFRESH_TOKEN, tokenId, tokenPair, login);

        String newAccessToken = tokenPair.getLeft().getTokenValue();
        String newRefreshToken = tokenPair.getRight().getTokenValue();
        return new AuthenticationResponse(newAccessToken, newRefreshToken);
    }

    private Pair<Jwt, Jwt> createAccessAndRefreshTokenPair(final RegisteredClient registeredClient,
                                                           final Profile profile, final String tokenId) {
        Jwt accessToken = createAccessToken(registeredClient, profile, tokenId);
        Jwt refreshToken = createRefreshToken(registeredClient, profile, tokenId);

        return Pair.of(accessToken, refreshToken);
    }

    private void saveOAuth2Authorization(final RegisteredClient registeredClient,
                                         final AuthorizationGrantType authorizationGrantType, final String tokenId,
                                         final Pair<Jwt, Jwt> tokenPair, final String login) {

        Jwt accessToken = tokenPair.getLeft();
        Jwt refreshToken = tokenPair.getRight();
        Set<String> scopes = registeredClient.getScopes();
        OAuth2AccessToken oAuth2AccessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER,
                accessToken.getTokenValue(), accessToken.getIssuedAt(), accessToken.getExpiresAt(), scopes);
        OAuth2RefreshToken oAuth2RefreshToken
                = new OAuth2RefreshToken(refreshToken.getTokenValue(),
                refreshToken.getIssuedAt(), refreshToken.getExpiresAt());

        OAuth2Authorization oAuth2Authorization = OAuth2Authorization
                .withRegisteredClient(registeredClient)
                .id(tokenId)
                .accessToken(oAuth2AccessToken)
                .refreshToken(oAuth2RefreshToken)
                .authorizationGrantType(authorizationGrantType)
                .principalName(login)
                .build();

        oAuth2AuthorizationService.save(oAuth2Authorization);
    }

    private Jwt createAccessToken(final RegisteredClient registeredClient, final Profile profile, final String tokenId) {
        String username = profile.getLogin();
        Long profileId = profile.getId();
        Role role = profile.getRole();
        long accessTokenTtl = registeredClient.getTokenSettings().getAccessTokenTimeToLive().toMillis();
        Set<String> scopes = registeredClient.getScopes();
        String clientId = registeredClient.getClientId();
        JwtClaimsSet jwtClaimsSet = JwtClaimsSet.builder()
                .id(tokenId)
                .issuer(clientId)
                .issuedAt(Instant.now())
                .expiresAt(Instant.now().plusMillis(accessTokenTtl))
                .claim(OAuth2ParameterNames.SCOPE, scopes)
                .claim(USERNAME, username)
                .claim(PROFILE_ID, profileId)
                .claim(ROLE, role)
                .claim(OAuth2ParameterNames.TOKEN_TYPE, OAuth2ParameterNames.ACCESS_TOKEN)
                .build();

        JwtEncoderParameters jwtEncoderParameters = JwtEncoderParameters.from(JWS_HEADER, jwtClaimsSet);
        return jwtEncoder.encode(jwtEncoderParameters);
    }

    private Jwt createRefreshToken(final RegisteredClient registeredClient, final Profile profile, final String tokenId) {
        Long profileId = profile.getId();
        Role role = profile.getRole();
        Set<String> scopes = registeredClient.getScopes();
        long accessTokenTtl = registeredClient.getTokenSettings().getAccessTokenTimeToLive().toMillis();
        String clientId = registeredClient.getClientId();
        JwtClaimsSet jwtClaimsSet = JwtClaimsSet.builder()
                .id(tokenId)
                .issuer(clientId)
                .issuedAt(Instant.now())
                .expiresAt(Instant.now().plusMillis(accessTokenTtl))
                .claim(OAuth2ParameterNames.SCOPE, scopes)
                .claim(PROFILE_ID, profileId)
                .claim(ROLE, role)
                .claim(OAuth2ParameterNames.TOKEN_TYPE, OAuth2ParameterNames.REFRESH_TOKEN)
                .build();

        JwtEncoderParameters jwtEncoderParameters = JwtEncoderParameters.from(JWS_HEADER, jwtClaimsSet);
        return jwtEncoder.encode(jwtEncoderParameters);
    }

    private void checkRegisterClient(final RegisteredClient registeredClient,
                                     final AuthorizationGrantType authorizationGrantType) {
        if (registeredClient == null) {
            log.debug("Failed to authenticate since password does not match stored value");
            throw new BadCredentialsException("Bad credentials");
        }
        if (!registeredClient.getAuthorizationGrantTypes().contains(authorizationGrantType)) {
            log.debug("Failed to authenticate since password does not match stored value");
            throw new BadCredentialsException("Bad credentials");
        }
    }

    private void checkPassword(final String presentedPassword, final String currentPassword) {
        if (!passwordEncoder.matches(presentedPassword, currentPassword)) {
            log.debug("Failed to authenticate since password does not match stored value");
            throw new BadCredentialsException("Bad credentials");
        }
    }
}
