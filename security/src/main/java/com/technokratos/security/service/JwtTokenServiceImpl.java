package com.technokratos.security.service;

import com.technokratos.security.dto.AccountResponse;
import com.technokratos.security.dto.Role;
import com.technokratos.security.dto.TokenCoupleRequest;
import com.technokratos.security.dto.TokenCoupleResponse;
import com.technokratos.security.dto.TokenRequest;
import com.technokratos.security.security.provider.JwtAccessTokenProvider;
import com.technokratos.security.security.provider.JwtRefreshTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JwtTokenServiceImpl implements JwtTokenService {

    private final JwtAccessTokenProvider jwtAccessTokenProvider;
    private final JwtRefreshTokenProvider jwtRefreshTokenProvider;

    @Override
    public AccountResponse getUserInfoByToken(TokenRequest token) {
        return jwtAccessTokenProvider.userInfoByToken(token);
    }

    @Override
    public TokenCoupleResponse generateTokenCouple(AccountResponse accountResponse) {
        String accessToken = jwtAccessTokenProvider.generateAccessToken(
                accountResponse.getLogin(),
                Collections.singletonMap(ROLE, accountResponse
                        .getRole())
        );
        String refreshToken = jwtRefreshTokenProvider.generateRefreshToken(accountResponse);
        return TokenCoupleResponse.builder()
                .accessToken(BEARER.concat(" ").concat(accessToken))
                .refreshToken(refreshToken)
                .build();
    }

    @Override
    public TokenCoupleResponse refreshAccessToken(TokenCoupleRequest tokenCoupleDto) {
        List<String> roles = jwtAccessTokenProvider.getRolesFromAccessToken(tokenCoupleDto.getAccessToken().replace(BEARER.concat(" "), ""));
//        RefreshTokenEntity verifiedRefreshToken = jwtRefreshTokenProvider.verifyRefreshTokenExpiration(
//                tokenCoupleDto.getRefreshToken(), roles
//        );

        String accessToken = jwtAccessTokenProvider.generateAccessToken(
                jwtAccessTokenProvider.getSubjectFromAccessToken(tokenCoupleDto.getAccessToken().replace(BEARER.concat(" "), "")),
                Collections.singletonMap(ROLE, roles));
        return TokenCoupleResponse.builder()
                .refreshToken(String.valueOf(UUID.randomUUID()))
                .accessToken(BEARER.concat(" ").concat(accessToken))
                .build();
    }
}