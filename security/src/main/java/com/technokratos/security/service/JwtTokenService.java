package com.technokratos.security.service;

import com.technokratos.security.dto.AccountResponse;
import com.technokratos.security.dto.TokenCoupleRequest;
import com.technokratos.security.dto.TokenCoupleResponse;
import com.technokratos.security.dto.TokenRequest;

public interface JwtTokenService {

    AccountResponse getUserInfoByToken(TokenRequest token);

    TokenCoupleResponse generateTokenCouple(AccountResponse accountResponse);

    TokenCoupleResponse refreshAccessToken(TokenCoupleRequest tokenCoupleRequest);
}