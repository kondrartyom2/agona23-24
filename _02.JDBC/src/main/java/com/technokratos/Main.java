package com.technokratos;

import com.technokratos.dto.UserEntity;
import com.technokratos.utill.RowMapper;
import com.technokratos.utill.UserRowMapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Main {

    //language=sql
    public final static String SQL_SAVE = "insert into account(name, age) values ('%s', %s)";
    //language=sql
    public final static String SQL_SAVE_PREP = "insert into account(name, age) values(?, ?)";
    //language=sql
    public final static String SQL_GET_BY_ID = "select * from account where id = %s";
    //language=sql
    public final static String SQL_GET_BY_ID_PREP = "select * from account where id = ?";
    //language=sql
    public final static String SQL_UPDATE = "update account set name = ? where id = ?";

    public static void main(String[] args) {
        final String HOST = "jdbc:postgresql://localhost:5432/agona-db";
        final String USER = "postgres";
        final String PASS = "password";
        try (Connection connection = DriverManager.getConnection(HOST, USER, PASS)) {
            Statement statement = connection.createStatement();
            statement.executeUpdate(
                    SQL_SAVE.formatted("artem", 20)
            );
            ResultSet resultSet = statement.executeQuery(
                    SQL_GET_BY_ID.formatted(3)
            );
            PreparedStatement prepStatement = connection.prepareStatement(SQL_GET_BY_ID_PREP);
            prepStatement.setInt(1, 3);
            ResultSet resultPrepSet = statement.getResultSet();
            RowMapper<UserEntity> rowMapper = new UserRowMapper();
            Boolean next = resultSet.next();
            List<UserEntity> userEntities = new ArrayList<>();
            int i = 0;
            while (next) {
                userEntities.add(rowMapper.from(resultPrepSet, i));
                next = resultPrepSet.next();
                i++;
            }
            System.out.println();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Hello world!");
    }
}