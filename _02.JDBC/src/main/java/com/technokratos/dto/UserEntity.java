package com.technokratos.dto;

public record UserEntity(Long id, String name, Integer age) {
}
