package com.technokratos.repository;

public class UserRepositoryJdbc implements Repository {


    //language=sql
    public final static String SQL_SAVE = "insert into account(name, age) values(%s, %s)";
    //language=sql
    public final static String SQL_SAVE_PREP = "insert into account(name, age) values(?, ?)";
    //language=sql
    public final static String SQL_GET_BY_ID = "select * from account where id = ?";
    //language=sql
    public final static String SQL_UPDATE = "update account set name = ? where id = ?";
}
