package com.technokratos.repository;

import com.technokratos.dto.UserEntity;
import com.technokratos.utill.RowMapper;
import com.technokratos.utill.UserRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public interface Repository<T> {

    default List<T> extract(RowMapper<T> rowMapper, ResultSet resultSet) throws SQLException {
        Boolean next = resultSet.next();
        List<T> entities = new ArrayList<>();
        int i = 0;
        while (next) {
            entities.add(rowMapper.from(resultSet, i));
            next = resultSet.next();
            i++;
        }
        return entities;
    }
}
