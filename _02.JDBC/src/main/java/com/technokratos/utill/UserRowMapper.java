package com.technokratos.utill;

import com.technokratos.dto.UserEntity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<UserEntity> {
    @Override
    public UserEntity from(ResultSet rs, int rowNum) throws SQLException {
        return new UserEntity(
                rs.getLong("id"),
                rs.getString("name"),
                rs.getInt("age")
        );
    }
}
