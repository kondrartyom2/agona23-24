import java.util.Objects;
import java.util.UUID;

public class User {

    private String name;
    private UUID id;
    private Typr type;

    public User(String name, UUID id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (Objects.isNull(o)) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (o instanceof User user) {
            return Objects.equals(name, user.name) && Objects.equals(id, user.id);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
}
